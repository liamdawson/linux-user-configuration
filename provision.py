from __future__ import print_function, unicode_literals, absolute_import
import sys
sys.path.append('.')

from argparse import ArgumentParser
from importlib import import_module
from tdsc import DesiredStateContext
import states


def write_state_description(state):
    # return str(state)
    print("\x1b[1;32m" + state.name, file=sys.stderr)
    print("\x1b[0;32m" + state.description, file=sys.stderr)
    print("\x1b[0m", file=sys.stderr)


if __name__ == '__main__':
    parser = ArgumentParser(
        description='Provision machine to meet desired state.')
    parser.add_argument('tags', metavar='TAG', nargs='+',
                        help='State tags to apply (e.g. "ubuntu", "xps15").')
    parser.add_argument(
        '--apply', help='Apply states, rather than listing states to apply.', action='store_true')

    args = parser.parse_args()

    context = DesiredStateContext(sys.stderr)
    context.tags = set([tag.lower() for tag in args.tags])

    states = states.state_config.filter(context)

    if args.apply:
        for state in states:
            write_state_description(state)

            if not state.should_run(context):
                print("Action should_run returned false, skipping.")
                continue

            if not state.apply(context):
                print("Previous action failed. Exiting.")
                break
    else:
        print('States to apply:\n', file=sys.stderr)
        for state in states:
            write_state_description(state)
