import tdsc
from . import gnome, pyenv, rbenv, nodenv

states = [] + gnome.states + pyenv.states + rbenv.states + nodenv.states

def relevant_states(context):
    return filter(lambda state: state.should_apply(context), states)

state_config = tdsc.DesiredStateReducer(states)
