import os
from tdsc import git, primatives


class ClonePyenvIfMissing(git.CloneIfMissingState):
    """Ensure pyenv is cloned locally."""
    source = 'https://github.com/pyenv/pyenv.git'
    destination = os.path.expandvars('$HOME/.pyenv')

    def should_apply(self, _):
        return True


class SetPyenvGlobalVersion(primatives.TemplateState):
    """Ensure preferred Python version is set globally for user."""
    destination = os.path.expandvars('$HOME/.python-version')
    template = '3.7.0'
    args = {}

    def should_apply(self, _):
        return True


states = [
    ClonePyenvIfMissing(),
    SetPyenvGlobalVersion()
]
