import tdsc

class DisableNaturalScroll(tdsc.gnome.SetGsettingState):
    """Disable touchpad 'natural scroll' in Gnome."""
    tags = [set(['xps15', 'ubuntu'])]
    namespace = 'org.gnome.desktop.peripherals.touchpad'
    setting = 'natural-scroll'
    value = 'false'

class EnableTapToClick(tdsc.gnome.SetGsettingState):
    """Enable tap to click on touchpad."""
    tags = [set(['xps15', 'ubuntu'])]
    namespace = 'org.gnome.desktop.peripherals.touchpad'
    setting = 'tap-to-click'
    value = 'true'

class ShowBatteryPercentageIndicator(tdsc.gnome.SetGsettingState):
    """Enable battery percentage indicator."""
    tags = [set(['xps15', 'ubuntu'])]
    namespace = 'org.gnome.desktop.interface'
    setting = 'show-battery-percentage'
    value = 'true'

class SetArcDarkerTheme(tdsc.gnome.SetGsettingState):
    """Set the GTK theme to Arc Darker."""
    tags = [set(['xps15', 'ubuntu'])]
    namespace = 'org.gnome.desktop.interface'
    setting = 'gtk-theme'
    value = '"Arc-Darker"'

states = [
    DisableNaturalScroll(),
    EnableTapToClick(),
    ShowBatteryPercentageIndicator(),
    SetArcDarkerTheme()
]
