import os
from tdsc import git, primatives

class CloneNodenvIfMissing(git.CloneIfMissingState):
    """Ensure nodenv is cloned locally."""
    source = 'https://github.com/nodenv/nodenv.git'
    destination = os.path.expandvars('$HOME/.nodenv')

    def should_apply(self, _):
        return True

class CloneNodenvBuildIfMissing(git.CloneIfMissingState):
    """Ensure nodenv node-build is cloned locally."""
    source = 'https://github.com/nodenv/node-build.git'
    destination = os.path.expandvars('$HOME/.nodenv/plugins/node-build')

    def should_apply(self, _):
        return True


class SetNodenvGlobalVersion(primatives.TemplateState):
    """Ensure preferred Node version is set globally for user."""
    destination = os.path.expandvars('$HOME/.node-version')
    template = '10.5.0'
    args = {}

    def should_apply(self, _):
        return True

states = [
    CloneNodenvIfMissing(),
    CloneNodenvBuildIfMissing(),
    SetNodenvGlobalVersion()
]
