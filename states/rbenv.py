import os
from tdsc import git, primatives


class CloneRbenvIfMissing(git.CloneIfMissingState):
    """Ensure rbenv is cloned locally."""
    source = 'https://github.com/rbenv/rbenv.git'
    destination = os.path.expandvars('$HOME/.rbenv')

    def should_apply(self, _):
        return True


class CloneRbenvBuildIfMissing(git.CloneIfMissingState):
    """Ensure rbenv ruby-build is cloned locally."""
    source = 'https://github.com/rbenv/ruby-build.git'
    destination = os.path.expandvars('$HOME/.rbenv/plugins/ruby-build')

    def should_apply(self, _):
        return True


class SetRbenvGlobalVersion(primatives.TemplateState):
    """Ensure preferred Ruby version is set globally for user."""
    destination = os.path.expandvars('$HOME/.ruby-version')
    template = '2.5.1'
    args = {}

    def should_apply(self, _):
        return True


states = [
    CloneRbenvIfMissing(),
    CloneRbenvBuildIfMissing(),
    SetRbenvGlobalVersion()
]
